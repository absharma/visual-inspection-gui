The GUI is visually optimised for MacOS and is now being converted for use on CentOS7.

Installation Instructions for CentOS 7:

Install python 3.6:

`sudo yum update`

`sudo yum install python36`


install pip3:

`sudo yum install python36-setuptools`

`sudo easy_install pip`


install PyQT5:

`yum install python36-qt5-devel`

install pdflatex and all required tex packages:

`sudo yum install texlive-*`


Comprehensive documentation of the operation of the GUI can be found on the following: https://edms.cern.ch/document/2392322/1