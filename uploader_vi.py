#!/usr/bin/env python3
# -*- coding: utf-8 -*
######################################################
## Author1: Shohei Shirabe (shohei.shirabe at cern.ch)
## Copyright: Copyright 2019, ldbtools
## Date: Apr. 2020
## Project: Local Database Tools
## Description: ITkPD Interface
######################################################

import os, sys, getpass, itkdb
from upload_results import *

####################################
## upload test results

def upload():

    paths = sys.argv
            
    print('ITKDB_ACCESS_CODE1:')
    code1 = getpass.getpass()
    print('ITKDB_ACCESS_CODE2:')
    code2 = getpass.getpass()    
    token = process_request(code1,code2)

    if token == 0:
        sys.exit(1)
    else:
        #if len(paths) > 1:
        #    for path in paths[1:]:
        #        with open(path) as f:
        #            next(f)
        f = open("/Users/abhisheksharma/Desktop/Visual_Inspection_GUI/sample_results.txt","r")
        lines = f.readlines()
        count=0
        for line in lines:
            count+=1
            if count==1: continue
            print(line)
            mod_name,stage,run_num,date,result1,result2 = line.split()
            upload_results(code1,code2,mod_name,stage,run_num,date,result1,result2)

        else:
            print("path to results are not set")

def process_request(code1,code2):
    try:
        u = itkdb.core.User(accessCode1=code1, accessCode2=code2)
        pd_client = itkdb.Client(user=u)
        docs = pd_client.get('listInstitutions', json={})
        print('Authorized.')        
        request = 1
    except:
        print('Not authorized. Please login for ITkPD by using itkpd-interface/authenticate.sh')        
        request = 0
    return request
            
if __name__ == '__main__': upload()

